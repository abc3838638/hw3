package exercise3;
/*
 * author 郭亮興
 */


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import com.google.gson.Gson;

/*
 * author 郭亮興
 */

public class Server {

	private ServerSocket serverSocket;
	private final int ServerPort = 6666;
	Socket clientSocket = null;
	int num = 0;
	
	public ArrayList<Socket> socketList = new ArrayList<Socket>();

	public Server() {

		try {
			serverSocket = new ServerSocket(ServerPort);
			System.out.println("waiting....");
			while (true) {
				clientSocket = serverSocket.accept();
				//output.add(writer);
				System.out.println("connect");
				socketList.add(clientSocket);
				PrintStream out = new PrintStream(clientSocket.getOutputStream());
				out.println(num);
				
				//
				
				new Thread(new SocketThread(clientSocket,socketList)).start();
			}

		} catch (IOException e) {
			System.out.println("Socket啟動有問題 !");
			System.out.println("IOException :" + e.toString());
		}
	}

	public static void main(String[] args) {
		new Server();
	}
	

	class SocketThread implements Runnable {
		Socket client;
		public ArrayList<Socket> socketList;

		public SocketThread(Socket s,ArrayList<Socket> socketList) {
			client = s;
			this.socketList=socketList;
		}
		
		public void broadcast(){
			
			for(Socket s:socketList){
				
				try {
					PrintStream out = new PrintStream(s.getOutputStream());
					Request r = new Request(Request.ANS,num);
					Gson gson = new Gson();					
					out.println(gson.toJson(r));
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}

		public void run() {

			try {
				PrintStream out = new PrintStream(client.getOutputStream());

				BufferedReader in = new BufferedReader(new InputStreamReader(
						client.getInputStream()));
				try {

					//Gson gson = new Gson();

					//out.println(gson.toJson(new Response(num)));
					//out = null;

					String message;

					while (true) {
						message = in.readLine();

						Request req = new Gson().fromJson(message,
								Request.class);

						System.out.println("the client comes message :" + message);

						
						if(req.operators.equals(Request.ADD)){
							num=num+req.number;
							System.out.println(num);
						}
						else if(req.operators.equals(Request.SUB)){
							num=num-req.number;
						}

						// add

						// substruct

						// out.writeInt(str);
						
						broadcast();
						
					}
				} catch (EOFException e) {
					System.out.println("error:" + e.getMessage());
				}
				out.close();
				in.close();
				client.close();
			} catch (IOException e) {
				System.out.println("Socket連線有問題 !");
				System.out.println("IOException :" + e.toString());
			}

		}
	}

}