package exercise3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;










import com.google.gson.Gson;

import java.awt.Font;
import java.awt.Label;
import java.awt.TextField;
import java.awt.Button;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientUI extends JFrame {

	private JPanel contentPane;
	private Label iplabel = new Label();
	private Label portlabel = new Label();
	private TextField ipname = new TextField();
	private TextField portnum = new TextField();
	private Label inputlabel = new Label();
	private TextField inputnum = new TextField();
	private Button connectbutton = new Button();
	private Button disconnectbutton = new Button();
	private Button addbutton = new Button();
	private Button subtractbutton = new Button();
	private List outputList = new List();
	private Socket socketToServer;
	public PrintStream out = null;
	public BufferedReader in = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientUI frame = new ClientUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ClientUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		iplabel.setText("IP");
		iplabel.setFont(new Font("Dialog", Font.PLAIN, 16));
		iplabel.setBounds(10, 10, 25, 23);
		contentPane.add(iplabel);
		ipname.setBounds(36, 10, 140, 23);
		ipname.setText("localhost");
		contentPane.add(ipname);

		portlabel.setText("Port:");
		portlabel.setFont(new Font("Dialog", Font.PLAIN, 16));
		portlabel.setBounds(189, 10, 37, 23);
		contentPane.add(portlabel);

		portnum.setBounds(232, 10, 80, 23);
		portnum.setText("6666");
		contentPane.add(portnum);

		connectbutton.setLabel("Connect");
		connectbutton.setFont(new Font("Dialog", Font.PLAIN, 15));
		connectbutton.setBounds(318, 10, 93, 23);
		contentPane.add(connectbutton);
		connectbutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				connectActionPerformed(e);
			}
		});

		inputlabel.setText("Number");
		inputlabel.setFont(new Font("Dialog", Font.PLAIN, 16));
		inputlabel.setBounds(68, 53, 63, 23);
		contentPane.add(inputlabel);

		inputnum.setBounds(137, 53, 164, 23);
		contentPane.add(inputnum);

		disconnectbutton.setLabel("Disconnect");
		disconnectbutton.setFont(new Font("Dialog", Font.PLAIN, 16));
		disconnectbutton.setBounds(318, 53, 93, 23);
		contentPane.add(disconnectbutton);
		disconnectbutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				disconnectActionPerformed(e);
			}
		});

		addbutton.setLabel("Add");
		addbutton.setFont(new Font("Dialog", Font.PLAIN, 16));
		addbutton.setBounds(54, 88, 100, 23);
		contentPane.add(addbutton);
		addbutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addActionPerformed(e);
			}
		});

		outputList.setBounds(52, 127, 249, 125);
		contentPane.add(outputList);

		subtractbutton.setLabel("Subtract");
		subtractbutton.setFont(new Font("Dialog", Font.PLAIN, 16));
		subtractbutton.setBounds(189, 88, 112, 23);
		contentPane.add(subtractbutton);
		subtractbutton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				subtractActionPerformed(e);
			}
		});
	}

	private void showWarningMessageDialog(String message) {
		JOptionPane.showMessageDialog(null, message, "Alert",
				JOptionPane.WARNING_MESSAGE);
	}

	// Overridden so we can exit when window is closed
	protected void processWindowEvent(WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == WindowEvent.WINDOW_CLOSING) {
			System.exit(0);
		}
	}

	private void connectActionPerformed(ActionEvent e) {
		String hostname = ipname.getText();
		int port = Integer.parseInt(portnum.getText());

		if (socketToServer != null) {
			showWarningMessageDialog("Please interrupt the current connection.\nPress End first.");

		} else {

			try {
				socketToServer = new Socket(hostname, port);
				out = new PrintStream(socketToServer.getOutputStream());
				in = new BufferedReader(new InputStreamReader(socketToServer.getInputStream()));
				String str = in.readLine();
				outputList.add(str);
				new Thread(new ListenReader()).start();
			} catch (UnknownHostException e1) {
				showWarningMessageDialog("UnknownHost!");
			} catch (NumberFormatException f) {
				showWarningMessageDialog("port between 0 and 65535.");
				System.out.println("Error:" + f.getMessage());
			} catch (IOException e1) {
				showWarningMessageDialog(e1.getMessage());
			}
		}

	}

	private void disconnectActionPerformed(ActionEvent e) {
		try {
			outputList.removeAll();
			out.close();
			in.close();
			socketToServer.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		out = null;
		in = null;
		socketToServer = null;
		System.out.println("disconnected");

	}

	private void addActionPerformed(ActionEvent e) {
		if(socketToServer!=null){
			try{
			    int number = Integer.parseInt(inputnum.getText());
                
                Request r = new Request(Request.ADD,number);
				
				Gson gson = new Gson();
				String json = gson.toJson(r);  
				
				System.out.println(json);
				out.println(json);
				
			}
			
			catch(NumberFormatException f){
				showWarningMessageDialog("Please input a integer");
			}
		
		}
		else{
			showWarningMessageDialog("Disconnect");
			
		}

	}

	private void subtractActionPerformed(ActionEvent e) {
		if(socketToServer!=null){
			try{
				int number = Integer.parseInt(inputnum.getText());
				Request r = new Request(Request.SUB,number);
				Gson gson = new Gson();
				String json = gson.toJson(r);  
				
				System.out.println(json);
				out.println(json);
				
			}
			
			catch(NumberFormatException f){
				showWarningMessageDialog("Please input a integer");
			}
		
		}
		else{
			showWarningMessageDialog("Disconnect");
			
		}

	}
	class ListenReader implements Runnable{
		  public void run(){
			  try{
				  while(true){
					  String str = in.readLine();
					  
					  Request req = new Gson().fromJson(str,
								Request.class);					
					  System.out.println(str);
					  outputList.add(Integer.toString(req.number));
				  }
			  }
			  catch(Exception e ){
				  e.printStackTrace();
			  }  
		  }
	} 

}
